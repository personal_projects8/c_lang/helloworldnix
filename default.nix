with import <nixpkgs> {}; {
	qpidEnv = stdenvNoCC.mkDerivation {
		name = "my-gcc8-enviroment";
		buildInputs = [
			gcc8
			cmake
		];
	};
}
